; fizzbuzz program writen in x86_64 bit assembly
; assembles with NASM using only 32 bit registers
%include	'func_fizzbuzz.asm'	; include external code from file

SECTION .data
;variables
fizz:	db	'fizz', 0h
buzz:	db	'buzz', 0h

SECTION .text

global _start

_start:

    mov    ecx, 0x0       ;init counter
increment_loop:
    mov    esi, 0x0       ; set esi to 0
    inc    ecx            ;increment loop counter
    cmp    ecx, 0x64      ;compare val in ecx to 100
    jg     end            ;if greater than 100, jump to end of program

check_fizz:
    mov    edx, 0x0       ;edx is 0, will store remainder
    mov    eax, ecx       ;store counter in eax
    mov    ebx, 0x3       ;move 3 into ebx as divisor
    div    ebx            ;eax/ebx
    cmp    edx,  0x0      ;compare remainder to 0
    je    print_fizz      ;if equal print fizz
    jne    check_buzz     ;if not jump to check_buzz
check_buzz:
    mov    edx, 0x0       ;reset edx
    mov    eax, ecx       ;put the counter back in ecx for next check
    mov    ebx, 0x5       ;store 5 in ebx
    div    ebx            ;eax/ebx 
    cmp    edx, 0x0       ;check if remainder 0
    je     print_buzz     ;if it is print buzz
    jne    check_num      ;if not check if its a number
check_num:
    cmp    esi, 0x0       ;cmp esi to 0
    jne    print_lf
    je     print_num      ;print number if nothing else has printed
print_num:
    mov    eax, ecx       ;mov ecx into eax to print
    call   iprintLF       ;print it
    jmp    increment_loop ;restart the loop
    
print_fizz:
    mov    eax, fizz      ;mov fizz into eax to print
    call   sprint         ;print the string
    inc    esi            ;increment esi
    jmp    check_buzz     ;jump to check_buzz
    
print_buzz:
    mov    eax, buzz      ;mov buzz into eax to print
    call   sprintLF       ;print it
    jmp    increment_loop ;restart the loop

print_lf:
    mov   eax, 0Ah        ;mov linefeed (end line) into eax
    push  eax             ;push onto the stack for print function
    mov   eax, esp        ;mov stack pointer into eax for print func
    call  sprint          ;call print function
    pop   eax             ;pop the value back onto the stack to clean up
    jmp increment_loop    ;restart the loop
    
end:
   call quit              ;exit program
