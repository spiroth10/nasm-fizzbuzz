; some functions to include as a library

; int slen(string message)
; calculates length of string message

slen:
	push	ebx ; push value in ebx onto the stack
	mov		ebx, eax ; copy pointer in eax to ebx

nextchar:
	cmp		byte [eax], 0 ; check if the current element of the string in eax is 0
	jz		finished	  ; jump to function finished if it is 0
	inc		eax			  ; go to next element in string
	jmp		nextchar	  ; jump back to beginning of nextchar

finished:
	sub		eax, ebx	; subtract the value of ebx from eax
	pop		ebx			; pop the value put on the stack back into ebx
	ret			    	; return to calling statement

; void sprint(string message)
; prints string message onto screen

sprint:
	push	edx			; push value of edx onto stack
	push	ecx			; push value of ecx onto stack
	push	ebx			; ..ebx
	push	eax			; ..eax
	call	slen		; call slen function

	mov		edx, eax	; copy eax to edx
	pop		eax			; pop value from stack back to eax

	mov		ecx, eax	; copy eax to ecx
	mov		ebx, 1		; write string to STDOUT
	mov		eax, 4		; syscall to SYS_WRITE to display STDOUT
	int		80h			; UNIX syscall interrupt 80h

	;pop rest of values off of stack and back to original registers 
	pop		ebx
	pop		ecx
	pop		edx

	;return to calling statement
	ret

; void sprintLF(string message)
; prints message and then a line feed after

sprintLF:
	call	sprint		; call to sprint function
	push	eax			; push value from eax onto stack
	mov		eax, 0Ah	; move a line feed into eax
	push	eax			; push the line feed onto stack
	mov		eax, esp	; move the address of the current stack pointer into eax for sprint
	call	sprint		; print the line feed
	pop		eax			; remove the linefeed from the stack
	pop		eax			; restore the original value of eax before function call
	ret					; return to calling statement

;void printI(int)
; prints an integer value to the screen with no line feed 
iprint:
    ;push contents of registers used onto stack
    push    eax 
    push    ecx
    push    edx
    push    esi
    ;counter for number of bytes to print
    mov     ecx, 0
; division loop, part of printI
; converts integer to ascii value
divLoop:
    inc     ecx     ;inc loop ctr
    mov     edx, 0  ;set edx to 0
    mov     esi, 10 ;move 10 into esi
    idiv    esi     ;divides int val in eax by val in esi
    add     edx, 48 ;adds int 48 to the value to get ascii, edx = remainder
    push    edx     ;push edx onto stack (the char/string)
    cmp     eax, 0  ;checks if eax is 0 
    jnz     divLoop ;restart loop if not zero
;printloop
;prints the values off the stack from divLoop
printLoop:
    dec     ecx         ;count down the loop counter
    mov     edx, 0
    mov     eax, esp    ;move stack pointer to eax
    call    sprint      ;call print function
    pop     eax         ;pop value off the stack
    cmp     ecx,0       ;compare counter to 0
    jnz     printLoop   ;if not 0, print next char
    ;restore the stack
    pop     esi
    pop     edx
    pop     ecx
    pop     eax
    ret

; void printILF(int)
; prints an integer with a line feed at the end
iprintLF:
    call   iprint ;call print integer function
    push   eax    ;store eax on stack
    mov    eax, 0Ah ;move line feed onto eax
    push   eax      ;push line feed onto stack
    mov    eax, esp ;put stack pointer in eax for print function
    call   sprint   ;print the line feed
    ;restore the stack
    pop    eax
    pop    eax
    ;return from function
    ret
    
	
; int GetRemainder()
; performs the equivalent of modulo in C
GetRemainder:
    
	
; void exit()
; Exit program and restore resources

quit:
	mov		ebx, 0		; set return value to 0
	mov		eax, 1		; sys_exit opcode
	int		80h			; UNIX syscall interrupt 80h
	ret					; return to calling statement (actually since this just exits, we're done)
